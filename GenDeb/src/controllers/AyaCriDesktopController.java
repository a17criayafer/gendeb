/*
 * Controlador del fichero desktop
 * */
package controllers;

import models.AyaCriDesktopModel;
import views.AyaCriDataDesktop;

public class AyaCriDesktopController {

	/*
	 * Creamos dos instancias, una de la vista y otra del modelo 
	 * */
	private AyaCriDesktopModel desktopModel;
	private AyaCriDataDesktop desktopView;
	
	/*
	 * Creadora del controlador, donde pasamos una instancia del modelo y otra de la vista
	 * */
	public AyaCriDesktopController(AyaCriDesktopModel desktopModel, AyaCriDataDesktop desktopView) {
		this.desktopModel = desktopModel;
		this.desktopView = desktopView;
	}
	
	/*
	 * Método que retorna true si los campos estan rellenados correctamente
	 * */
	public boolean comprobarCampos() {
		boolean todoOK = false;
		
		//Obtenemos la información que introduce el usuario de la pantalla
		String name = desktopView.getTxtDataDesktopNombreText();
		String category = desktopView.getTxtDataDesktopCategoriesText();
		String commentary = desktopView.getTxtDataDesktopComentarioText();
		String type = desktopView.getTxtDataDesktopTypeText();
		String execComand = desktopView.getTxtDataDesktopExecText();
		String terminal = desktopView.getTxtDataDesktopTerminal();
			
		//Asignamos los datos introducimos al modelo
		desktopModel.setNombre(name);
		desktopModel.setCategories(category);
		desktopModel.setComentario(commentary);
		desktopModel.setType(type);
		desktopModel.setExecComand(execComand);
		desktopModel.setTerminal(terminal);
		
		if (desktopModel.isCorrect()) {
			todoOK = true;
		}
		return todoOK;
	}
	
	/*
	 * Método que ejecuta el proceso concurrente para crear el archivo desktop
	 * */
	public void creacionFichero() {
		desktopModel.start();
	}

	/*
	 * Método toString
	 * */
	@Override
	public String toString() {
		return desktopModel.toString();
	}	
	
}
