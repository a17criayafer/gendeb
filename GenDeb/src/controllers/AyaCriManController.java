/*
 * Controlador del fichero man
 * */
package controllers;

import models.AyaCriManModel;
import views.AyaCriDataMan;

public class AyaCriManController {

	/*
	 * Creamos dos instancias, una de la vista y otra del modelo 
	 * */
	private AyaCriManModel manModel;
	private AyaCriDataMan manView;
	
	/*
	 * Creadora del controlador, donde pasamos una instancia del modelo y otra de la vista
	 * */
	public AyaCriManController (AyaCriManModel manModel, AyaCriDataMan manView) {
		this.manModel = manModel;
		this.manView = manView;
	}
	
	/*
	 * Método que retorna true si los campos estan rellenados correctamente
	 * */
	public boolean comprobarCampos() {
		boolean todoOK = false;
		
		//Obtenemos la información que introduce el usuario de la pantalla
		String sections = manView.getComboBoxDataManSections();
		String options = manView.getTxtDataManOptions();
		String description = manView.getTxtADataManDescription();
		String title = manView.getTxtDataManTitle();
		
		//Asignamos los datos introducimos al modelo
		manModel.setSections(sections);
		manModel.setOptions(options);
		manModel.setDescription(description);		
		manModel.setTitle(title);
	
		if (manModel.isCorrect()) {
			todoOK = true;
		}
		return todoOK;
	}
		
	/*
	 * Método que obtiene el número de la sección que selecciona el usuario
	 * */
	public char obtenerSection() {
		return manModel.getSections().charAt(0);
	}
	
	/*
	 * Método que ejecuta el proceso concurrente para crear el archivo desktop
	 * */
	public void creacionFichero() {
		manModel.start();
	}
	
	/*
	 * Método toString
	 * */
	public String toString() {
		return manModel.toString();
	}
	
}
