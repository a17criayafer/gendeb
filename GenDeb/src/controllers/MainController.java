package controllers;

import java.awt.CardLayout;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import models.VerCelControlModel;
import models.VerCelDataJarIconModel;
import models.VerVicDebDirectoryModel;
import models.AyaCriDesktopModel;
import models.VerVicDocModel;
import models.AyaCriManModel;
import views.AyaCriDataDesktop;
import views.VerVicWelcome;

public class MainController {

	//Atributo de la vista principal
	private VerVicWelcome mainView;
	
	//Atributos de la clase correspondientes a los controladores
	private AyaCriDesktopController desktopController;
	private AyaCriManController manController;
	private VerVicDocController docController;
	private VerVicDebDirectoryController debDirectoryController;
	private VerCelControlController controlControler;
	private VerCelDataJarIconController dataJarIconController;
	
	//Atributos de la clase correspondientes al modelo
	private AyaCriDesktopModel desktopModel;
	private AyaCriManModel manModel;
	private VerVicDocModel docModel;
	private VerVicDebDirectoryModel debDirectoryModel;
	private VerCelControlModel controlModel;
	private VerCelDataJarIconModel dataJarIconModel;
	
	/*
	 * Creadora del mainController donde se le pasa la vista principal de la aplicación
	 * */
	public MainController(VerVicWelcome mainView) {
		//Asignamos la vista
		this.mainView=mainView;
		
		//Inicializamos los modelos
		desktopModel = new AyaCriDesktopModel();
		manModel = new AyaCriManModel();
		docModel = new VerVicDocModel();
		debDirectoryModel = new VerVicDebDirectoryModel();
		controlModel = new VerCelControlModel();
		dataJarIconModel = new VerCelDataJarIconModel();
		
		//Inicializamos cada controlador con su modelo y vista correspondiente
		this.desktopController = new AyaCriDesktopController(desktopModel, mainView.getAyaCriDataDesktop());
		this.manController = new AyaCriManController(manModel, mainView.getAyaCriDataMan());
		this.docController = new VerVicDocController(docModel, mainView.getVerVicDataDoc());
		this.debDirectoryController = new VerVicDebDirectoryController(debDirectoryModel, mainView.getVerVicDondeGuardar());
		this.controlControler = new VerCelControlController(controlModel, mainView.getVerCelControl());
		this.dataJarIconController = new VerCelDataJarIconController(dataJarIconModel, mainView.getVerCelDataJarIcon());

		//Asignamos a cada modelo, los modelos extras que debe usar para obtener sus datos necesarios
		desktopModel.setDataJarIconModel(dataJarIconModel);
		desktopModel.setDebDirectoryModel(debDirectoryModel);
		desktopModel.setControlModel(controlModel);
		controlModel.setDebDirectoryModel(debDirectoryModel);		
		docModel.setDesktopModel(desktopModel);
		docModel.setControlModel(controlModel);
		docModel.setDebDirectoryModel(debDirectoryModel);		
		manModel.setControlModel(controlModel);
		manModel.setDesktopModel(desktopModel);
		manModel.setDebDirectoryModel(debDirectoryModel);		
		dataJarIconModel.setDebDirectoryModel(debDirectoryModel);
		dataJarIconModel.setControlModel(controlModel);
		
		//Ocultamos el botón de atrás
		mainView.getBtnBack().setVisible(false);
		
		//Añadimos las acciones correspondientes a los botones del Frame principal
		mainView.getBtnBack().addActionListener(e->back());
		mainView.getBtnNext().addActionListener(e->next());
		mainView.getBtnClose().addActionListener(e->close());
	}
	
	/*
	 * Método que retrocede un panel al pulsar el botón "BACK"
	 * */
	private void back() {
		CardLayout cl = (CardLayout) mainView.getCenterPanel().getLayout();
		JPanel centerPanel = mainView.getCenterPanel();
		
		mainView.cardActual -= 1;
		
		if(mainView.cardActual == 0) {
			cl.previous(centerPanel);	
			mainView.getBtnBack().setVisible(false);
		} else if(mainView.cardActual == 4) {
			cl.previous(centerPanel);
			mainView.getBtnNext().setText("Next ->");
		} else {
			cl.previous(centerPanel);
		}
	}

	/*
	 * Método que avanza un panel al pulsar el botón "NEXT"
	 * */
	private void next() {
		CardLayout cl = (CardLayout) mainView.getCenterPanel().getLayout();
		JPanel centerPanel = mainView.getCenterPanel();
		
		switch(mainView.cardActual) {
			case 0:
					//Se comprueba que todos los campos esten rellenos y se guardan en el modelo.
					if(controlControler.comprobarCampos()) {
						mainView.cardActual += 1;
						cl.next(centerPanel);
						mainView.getBtnBack().setVisible(true);
					}
					else {
						JOptionPane.showMessageDialog(null, "Please fill in all the fields.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					break;
			case 1:
					//Se comprueba que todos los campos esten rellenos y se guardan en el modelo.
					if(dataJarIconController.comprobarCampos()) {
						mainView.cardActual += 1;
						cl.next(centerPanel);
					}
					else {
						JOptionPane.showMessageDialog(null, "Please fill in all the fields.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					break;
			case 2:
					//Se comprueba que todos los campos esten rellenos y se guardan en el modelo.
					if(desktopController.comprobarCampos()) {
						mainView.cardActual += 1;	
						cl.next(centerPanel);
					}
					else {
						JOptionPane.showMessageDialog(null, "Please fill in all the fields.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					break;
			case 3:
					//Se comprueba que todos los campos esten rellenos y se guardan en el modelo.
					if(manController.comprobarCampos()) {
						mainView.cardActual += 1;
						cl.next(centerPanel);
					}
					else {
						JOptionPane.showMessageDialog(null, "Please fill in all the fields.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					break;
			case 4:
					//Se comprueba que todos los campos esten rellenos y se guardan en el modelo.
					if(docController.comprobarCampos()) {
						mainView.cardActual += 1;
						cl.next(centerPanel);
						mainView.getBtnNext().setText("Finish ->");
					}
					else {
						JOptionPane.showMessageDialog(null, "Please fill in all the fields.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					break;
			case 5:
					//Se comprueba que todos los campos esten rellenos y se guardan en el modelo.
					if(debDirectoryController.comprobarCampos()) {
						mainView.cardActual += 1;
						
						//Obtenemos el nombre del paquete y la sección del man para poder crear correctamente la estructura de carpetas
						String nombrePaquete = controlControler.obtenerNombrePaquete();
						char section = manController.obtenerSection();						
						debDirectoryController.creacionEstructuraCarpetas(nombrePaquete, section);
						
						//Una vez creada la estructura de carpetas, creamos los ficheros necesarios
						desktopController.creacionFichero();
						controlControler.creacionFichero();
						docController.creacionFichero();
						manController.creacionFichero();
						dataJarIconController.creacionFichero();
						
						//Acabado el proceso de creación de los ficheros, es necesario dar una serie de permisos y cambios de dueño de los ficheros
						try {
							assignPermissions(debDirectoryModel, desktopModel, controlModel);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						//Una vez creamos todos los ficheros, creamos el fichero md5sum
						try {
							debDirectoryController.crearMd5sum();
						} catch (IOException e) {
							e.printStackTrace();
						}
						
//						/*
//						 * NO FUNCIONA
//						 */
//						try {
//							assignOwner(debDirectoryModel);
//						} catch (IOException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
						
						//Acabado el proceso de asignar permisos y cambios de dueños, hay que ensamblar el paquete deb
						try {
							compressDeb(debDirectoryModel, controlModel);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						cl.next(centerPanel);
						
						//Escondemos los botones Next y Back cuando se ha generado el paquete deb y se ha llegado al card Finalizar
						mainView.getBtnBack().setVisible(false);
						mainView.getBtnNext().setVisible(false);
					}
					else {
						JOptionPane.showMessageDialog(null, "Please fill in all the fields.", "WARNING", JOptionPane.WARNING_MESSAGE);
					}
					break;
		}		
		mainView.getBtnBack().setEnabled(true);
	}
	
//	/*
//	 * REVISAR, NO FUNCIONA
//	 */
//	public void assignOwner(VerVicDebDirectoryModel debDirectoryModel) throws IOException {
//		String comando = "sudo chown root " + debDirectoryModel.getDirectory() + "/deb/*";
//		
//		ProcessBuilder pb = new ProcessBuilder("bash", "-c", comando);
//		Process p = pb.start();
//		try {
//			p.waitFor();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		p.exitValue();
//	}
	
	/*
	 * Método que asigna los permisos necesarios a los ficheros que lo necesitan
	 * */
	public void assignPermissions(VerVicDebDirectoryModel debDirectoryModel, AyaCriDesktopModel desktopModel, VerCelControlModel controlModel) throws IOException {
		String pathControlFile = debDirectoryModel.getDirectory() + "/deb/DEBIAN/control";
		String pathMd5sumsFile = debDirectoryModel.getDirectory() + "/deb/DEBIAN/md5sums";
		String pathDesktopFile = debDirectoryModel.getDirectory() + "/deb/usr/share/applications/" + desktopModel.getNombre() + ".desktop";
		String pathJarFile = debDirectoryModel.getDirectory() + "/deb/usr/share/" + controlModel.getPackageName();
		
		//Creamos un array para guardar todas las rutas de los ficheros a los que hay que cambiar los permisos
		ArrayList<String> pathFiles = new ArrayList<String>();
		pathFiles.add(pathControlFile);
		pathFiles.add(pathMd5sumsFile);
		pathFiles.add(pathDesktopFile);
		
		String comando;
		
		//Recorremos el array para obtener las rutas de los ficheros y les cambiamos los permisos
		for(int i = 0; i < pathFiles.size(); i++) {
			comando = "chmod 644 " + pathFiles.get(i);
			
			ProcessBuilder pb = new ProcessBuilder("bash", "-c", comando);
			Process p = pb.start();
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			p.exitValue();
		}
		
		//Asignar permisos al fichero jar
		comando = "chmod -R 755 " + pathJarFile;
		
		ProcessBuilder pb = new ProcessBuilder("bash", "-c", comando);
		Process p = pb.start();
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		p.exitValue();
		
		//Asignamos permisos al script
		String pathScriptFile = debDirectoryModel.getDirectory() + "/deb/usr/bin/";
		comando = "chmod -R 755 " + pathScriptFile;
		ProcessBuilder pbScript = new ProcessBuilder("bash", "-c", comando);
		Process pScript = pbScript.start();
		try {
			pScript.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		pScript.exitValue();
	}
	
	/*
	 * Método que ensambla el paquete deb
	 * */
	public void compressDeb(VerVicDebDirectoryModel debDirectoryModel, VerCelControlModel controlModel) throws IOException {		
		String packageName = controlModel.getPackageName();
		packageName = packageName.toLowerCase();
		String version = controlModel.getVersion();
		String architecture = controlModel.getArchitecture();
		String comando = "dpkg -b '" + debDirectoryModel.getDirectory() + "/deb' '" + debDirectoryModel.getDirectory() + "/" + packageName + "_" + version + "_" + architecture + ".deb'";
		
		ProcessBuilder pb = new ProcessBuilder("bash", "-c", comando);
		Process p = pb.start();
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		p.exitValue();
	}

	/*
	 * Método que cierra la aplicaciçon
	 * */
	public void close() {
		mainView.dispose();
	}
	
}
