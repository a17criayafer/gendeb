/*
 * Controlador del fichero de Control
 * */
package controllers;

import models.VerCelControlModel;
import views.VerCelControl;

public class VerCelControlController {
	/*
	 * Creamos dos atributos, una de la vista y otra del modelo correspondientes al fichero de Control
	 * */
	private VerCelControlModel controlModel;
	private VerCelControl controlView;
	
	/*
	 * Creadora del controlador, donde pasamos una instancia del modelo y otra de la vista
	 * */
	public VerCelControlController(VerCelControlModel controlModel, VerCelControl controlView) {
		this.controlModel = controlModel;
		this.controlView = controlView;
	}
	
	/*
	 * Métodos que miran si los campos estan rellenados correctamente o no
	 * */
	public boolean comprobarCampos() {
		boolean todoOK = false;
		
		//Obtenemos la información que introduce el usuario de la pantalla
		String paquete = controlView.getTxtPackage();
		String architecture = controlView.getTxtArchitecture();
		String seccion = controlView.getTxtSection();
		String priority = controlView.getTxtPriority();
		String manteinerName = controlView.getTxtMaintainerName();
		String manteinerEmail = controlView.getTxtMaintainerEmail();
		String homePage = controlView.getTxtHomePage();
		String dependences = controlView.getTxtDepends();
		String description = controlView.getTxtADescription();
		String version = controlView.getTxtVersion();
		String source = controlView.getTxtSource();
		String essential = controlView.getTxtEssential();
		
		//Asignamos los datos introducimos al modelo
		controlModel.setArchitecture(architecture);
		controlModel.setDependences(dependences);
		controlModel.setDescription(description);
		controlModel.setHomePage(homePage);
		controlModel.setManteinerName(manteinerName);
		controlModel.setManteinerEmail(manteinerEmail);
		controlModel.setPaquete(paquete);
		controlModel.setPriorityControl(priority);
		controlModel.setSeccion(seccion);
		controlModel.setVersion(version);
		controlModel.setSource(source);
		controlModel.setEssential(essential);
		
		if (controlModel.isCorrect()) {
			todoOK = true;
		}
		return todoOK;
	}
	
	/*
	 * Método que inicia la creación del fichero
	 * */
	public void creacionFichero() {
		controlModel.start();
	}
	
	/*
	 * Método que obtiene el nombre del paquete
	 * */
	public String obtenerNombrePaquete() {
		return controlModel.getPackageName();
	}
	
	/*
	 * Método toString
	 * */
	public String toString() {
		return controlModel.toString();
	}
	
}
