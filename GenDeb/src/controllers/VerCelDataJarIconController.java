/*
 * Controlador del panel que pide el fichero JAR y el icono para el fichero Desktop
 * */
package controllers;

import models.VerCelDataJarIconModel;
import views.VerCelDataJarIcon;

public class VerCelDataJarIconController {
	/*
	 * Creamos dos instancias, una de la vista y otra del modelo correspondientes al panel que pide el JAR y el icono
	 * */
	private VerCelDataJarIconModel dataJarIconModel;
	private VerCelDataJarIcon dataJarIconView;
	
	/*
	 * Creadora del controlador, donde pasamos una instancia del modelo y otra de la vista
	 * */
	public VerCelDataJarIconController(VerCelDataJarIconModel dataJarIconModel, VerCelDataJarIcon dataJarIconView) {
		this.dataJarIconModel = dataJarIconModel;
		this.dataJarIconView = dataJarIconView;
	}
	
	/*
	 * Métodos que miran si los campos estan rellenados correctamente o no
	 * */
	public boolean comprobarCampos() {
		boolean todoOK = false;
		
		//Obtenemos la información que introduce el usuario de la pantalla
		String iconDirectory = dataJarIconView.getTextFieldIcon();
		String jarDirectory = dataJarIconView.getTextFieldJarFile();
		
		//Asignamos los datos introducimos al modelo
		dataJarIconModel.setIconDirectory(iconDirectory);
		dataJarIconModel.setJarDirectory(jarDirectory);

		if (dataJarIconModel.isCorrect()) {
			todoOK = true;
		}
		return todoOK;
	}
	
	/*
	 * Método que ejecuta el proceso concurrente para crear el archivo desktop
	 * */
	public void creacionFichero() {
		dataJarIconModel.start();
	}

	/*
	 * Método toString de la clase
	 * */
	@Override
	public String toString() {
		return dataJarIconModel.toString();
	}	

}
