/*
 * Controlador del panel que pide la ruta para poder crear el paquete deb
 * */
package controllers;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import models.VerVicDebDirectoryModel;
import views.VerVicWhereToSave;

public class VerVicDebDirectoryController  {
	/*
	 * Creamos dos instancias, una de la vista y otra del modelo 
	 * */ 
	private VerVicDebDirectoryModel debDirectoryModel;
	private VerVicWhereToSave debDirectoryView;
	
	/*
	 * Creadora del controlador, donde pasamos una instancia del modelo y otra de la vista
	 * */
	public VerVicDebDirectoryController(VerVicDebDirectoryModel debDirectoryModel, VerVicWhereToSave debDirectoryView) {
		this.debDirectoryModel = debDirectoryModel;
		this.debDirectoryView = debDirectoryView;
		debDirectoryView.getBtnExplorar().addActionListener(e->explorarArchivos());
	}
	
	/*
	 * Método que selecciona el directorio
	 * */
	private void explorarArchivos() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Specify a file to open");    
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int userSelection = fileChooser.showOpenDialog(debDirectoryView.getBtnExplorar());
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    File fileToOpen = fileChooser.getSelectedFile();
		    debDirectoryView.setTextFieldDirectory(fileToOpen.getAbsolutePath());
		}
	}

	/*
	 * Métodos que miran si los campos estan rellenados correctamente o no
	 * */
	public boolean comprobarCampos() {
		boolean todoOK = false;
		
		//Obtenemos la información que introduce el usuario de la pantalla
		String directory = debDirectoryView.getTextFieldDirectory();
		
		//Asignamos los datos introducimos al modelo
		debDirectoryModel.setDirectory(directory);
		
		if (debDirectoryModel.isCorrect()) {
			todoOK = true;
		}
		return todoOK;
	}
	
	/*
	 * Método que crea los directorios y la estructura de carpetas necesarias para poder guardar los archivos y crear el paquete correctamente
	 * */
	public void creacionEstructuraCarpetas(String nombrePaquete, char section) {
		String directorio = debDirectoryModel.getDirectory();
		nombrePaquete = nombrePaquete.toLowerCase();
		
		//Se crean los directorios y la correspondiente estructura de carpetas
		new File(directorio + "/deb/DEBIAN").mkdirs();
		new File(directorio + "/deb/usr").mkdir();
		new File(directorio + "/deb/usr/bin").mkdir();
		new File(directorio + "/deb/usr/share").mkdir();
		new File(directorio + "/deb/usr/share/" + nombrePaquete).mkdir();
		new File(directorio + "/deb/usr/share/applications").mkdir();
		new File(directorio + "/deb/usr/share/doc").mkdir();
		new File(directorio + "/deb/usr/share/doc/" + nombrePaquete).mkdir();
		new File(directorio + "/deb/usr/share/man/").mkdir();
		new File(directorio + "/deb/usr/share/man/man" + section + "/").mkdir();
	}
	
	/*
	 * Método que creará el archivo md5sum
	 * */
	public void crearMd5sum() throws IOException {
		//find usr/ -type f -exec md5sum '{}' \; > DEBIAN/md5sums
		//en mac md5, linux md5sum
		
		String directorioDEBIAN = debDirectoryModel.getDirectory() + "/deb/DEBIAN/md5sums";
		
		File ficheroMD5SUM = new File(directorioDEBIAN);
		ficheroMD5SUM.createNewFile();
		
		if (ficheroMD5SUM.exists()) {
			String comando = "find usr/ -type f -exec md5sum '{}' \\; > " + directorioDEBIAN;
			ProcessBuilder pb = new ProcessBuilder("bash", "-c", comando);
			pb.directory(new File(debDirectoryModel.getDirectory() + "/deb/"));
			Process p = pb.start();
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			p.exitValue();	
		}	
		else {
			System.out.println("No existe el fichero");
		}
	}
	
	
	/*
	 * Método toString
	 * */
	public String toString() {
		return debDirectoryModel.toString();
	}
	
}
