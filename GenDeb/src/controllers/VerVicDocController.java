/*
 * Controlador del panel que pide el fichero copyright y demás
 * */
package controllers;

import java.io.File;

import javax.swing.JFileChooser;

import models.VerVicDocModel;
import views.VerVicDataDoc;

public class VerVicDocController {
	/*
	 * Creamos dos instancias, una de la vista y otra del modelo 
	 * */
	private VerVicDocModel docModel;
	private VerVicDataDoc docView;
	
	/*
	 * Creadora del controlador, donde pasamos una instancia del modelo y otra de la vista
	 * */ 
	public VerVicDocController(VerVicDocModel docModel, VerVicDataDoc docView) {
		this.docModel = docModel;
		this.docView = docView;
		docView.getBtnExplorar().addActionListener(e->explorarArchivos());
	}
	
	/*
	 * Método que selecciona el fichero copyright proporcionado por el usuario
	 * */
	private void explorarArchivos() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Specify a file to open");    

		int userSelection = fileChooser.showOpenDialog(docView.getBtnExplorar());
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    File fileToOpen = fileChooser.getSelectedFile();
		    docView.setTextFieldDirectory(fileToOpen.getAbsolutePath());
		}
	}

	/*
	 * Métodos que miran si los campos estan rellenados correctamente o no
	 * */
	public boolean comprobarCampos() {
		boolean todoOK = false;
		
		//Obtenemos la información que introduce el usuario de la pantalla
		String urgency = docView.getComboBoxUrgency();
		String distribution = docView.getComboBoxDistributions();
		String directory = docView.getTextFieldDirectory();
		String changes = docView.getTxtAChanges();
		
		//Asignamos los datos introducimos al modelo
		docModel.setUrgency(urgency);
		docModel.setDistributions(distribution);
		docModel.setDirectory(directory);
		docModel.setChanges(changes);
		
		if (docModel.isCorrect()) {
			todoOK = true;
		}
		return todoOK;
	}
	
	/*
	 * Método que ejecuta el proceso concurrente para crear el archivo desktop
	 * */
	public void creacionFichero() {
		docModel.start();
	}
	
	/*
	 * Método toString
	 * */
	public String toString() {
		return docModel.toString();
	}
	
}
