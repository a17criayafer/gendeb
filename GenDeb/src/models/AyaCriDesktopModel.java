package models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class AyaCriDesktopModel extends Thread implements Concurrencia{

	/*
	 * Atributos del fichero Desktop
	 * */
	private String name;
	private String commentary;
	private String execComand;
	private String type;
	private String categories;
	private String terminal;
	private VerCelDataJarIconModel dataJarIconModel;
	private VerVicDebDirectoryModel debDirectoryModel;
	private VerCelControlModel controlModel;

	/*
	 * Creadoras
	 * */
	public AyaCriDesktopModel() {
		
	}

	public AyaCriDesktopModel(String name, String commentary, String execComand, String type, String categories, String terminal) {
		this.name = name;
		this.commentary = commentary;
		this.execComand = execComand;
		this.type = type;
		this.categories = categories;
		this.terminal = terminal;
	}

	/*
	 * Getters de los atributos de la clase
	 * */
	public String getNombre() {
		return this.name;
	}
	
	public String getComentario() {
		return this.commentary;
	}
	
	public String getExecComand() {
		return this.execComand;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getCategories() {
		return this.categories;
	}
	
	public String getCommentary() {
		return commentary;
	}
	
	public String getTerminal() {
		return terminal;
	}
	
	public VerCelDataJarIconModel getDataJarIconModel() {
		return dataJarIconModel;
	}
	
	public VerVicDebDirectoryModel getDebDirectoryModel() {
		return debDirectoryModel;
	}
	
	public VerCelControlModel getControlModel() {
		return controlModel;
	}
	
	/*
	 * Setters de los atributos de la clase
	 * */

	public void setNombre(String name) {
		this.name = name;
	}

	public void setComentario(String commentary) {
		this.commentary = commentary;
	}

	public void setExecComand(String execComand) {
		this.execComand = execComand;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}	

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	
	public void setDataJarIconModel(VerCelDataJarIconModel dataJarIconModel) {
		this.dataJarIconModel = dataJarIconModel;
	}
	
	public void setDebDirectoryModel(VerVicDebDirectoryModel debDirectoryModel) {
		this.debDirectoryModel = debDirectoryModel;
	}
	
	public void setControlModel(VerCelControlModel controlModel) {
		this.controlModel = controlModel;
	}

	/*
	 * Método toString
	 * */
	@Override
	public String toString() {
		return "AyaCriDesktopModel [name=" + name + ", commentary=" + commentary + ", execComand=" + execComand
				+ ", type=" + type + ", categories=" + categories + ", terminal=" + terminal + ", dataJarIconModel="
				+ dataJarIconModel + ", debDirectoryModel=" + debDirectoryModel + "]";
	}

	@Override
	public boolean isCorrect() {
		if(!name.isEmpty() && !categories.isEmpty() && !commentary.isEmpty() && !type.isEmpty() && !execComand.isEmpty() && !terminal.isEmpty()) {
			if (!name.isBlank() && !categories.isBlank() && !commentary.isBlank() && !type.isBlank() && !execComand.isBlank() && !terminal.isBlank()) {
				return true;
			}
		}
		return false;
	}

	/*
	 * Método que crea el fichero desktop
	 * */
	public void createFile() throws IOException {
		//Generamos la ruta donde guardaremos el fichero
		String[] iconPath = this.dataJarIconModel.getIconDirectory().split("/");
		String debPath = debDirectoryModel.getDirectory() + "/deb/usr/share/applications/";
		File file = new File(debPath + controlModel.getPackageName().toLowerCase() + ".desktop");
		System.out.println(debPath + controlModel.getPackageName().toLowerCase() + ".desktop");
		
		//Generamos el contenido del fichero man
		String fileMsg = "[Desktop Entry]\n"
				+ "Name=" + this.name + "\n"
				+ "Comment=" + this.commentary + "\n"
				+ "Exec=" + controlModel.getPackageName().toLowerCase() + "\n"
				+ "Icon=" + "/usr/share/applications/" + iconPath[iconPath.length - 1] + "\n"
				+ "Type=" + this.type + "\n"
				+ "Categories=" + this.categories + ";";
		
		//Escribimos el contenido del fichero
		FileWriter writer = new FileWriter(file);
		writer.write(fileMsg);
		writer.close();
		
		//Copiamos el fichero icon adjunto
		File originPath = new File(this.dataJarIconModel.getIconDirectory());
		File finalPath = new File(debPath + iconPath[iconPath.length - 1]);
		Files.copy(Paths.get(originPath.getAbsolutePath()), Paths.get(finalPath.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
	}
	
	/*
	 * Método run que llama al createFile para usarlo con threads
	 * */
	@Override
	public void run() {
		try {
			createFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
