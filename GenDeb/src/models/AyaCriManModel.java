package models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AyaCriManModel extends Thread implements Concurrencia {

	/*
	 * Atributos para el fichero Man
	 * */
	
	private String sections;
	private String title;
	private String options;
	private String description;
	private VerCelControlModel controlModel;
	private VerVicDebDirectoryModel debDirectoryModel;
	private AyaCriDesktopModel desktopModel;

	/*
	 * Creadoras
	 * */
	
	public AyaCriManModel() {
		
	}
	
	public AyaCriManModel(String sections, String title, String options, String description) {
		this.sections = sections;
		this.title = title;
		this.options = options;
		this.description = description;
	}

	/*
	 * Getters de los atributos de la clase
	 * */
	
	public String getSections() {
		return sections;
	}

	public String getTitle() {
		return title;
	}
	
	public String getOptions() {
		return options;
	}

	public String getDescription() {
		return description;
	}

	public AyaCriDesktopModel getDesktopModel() {
		return desktopModel;
	}
	
	public VerVicDebDirectoryModel getDebDirectoryModel() {
		return debDirectoryModel;
	}
	
	public VerCelControlModel getControlModel() {
		return controlModel;
	}
	
	
	/*
	 * Setter de los atributos de la clase
	 * */
	
	public void setSections(String sections) {
		this.sections = sections;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setOptions(String options) {
		this.options = options;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setDesktopModel(AyaCriDesktopModel desktopModel) {
		this.desktopModel = desktopModel;
	}

	public void setDebDirectoryModel(VerVicDebDirectoryModel debDirectoryModel) {
		this.debDirectoryModel = debDirectoryModel;
	}

	public void setControlModel(VerCelControlModel controlModel) {
		this.controlModel = controlModel;
	}
	
	/*
	 * Método toString
	 * */

	@Override
	public String toString() {
		return "AyaCriManModel [sections=" + sections + ", title=" + title + ", options=" + options + ", description="
				+ description + ", controlModel=" + controlModel + ", debDirectoryModel=" + debDirectoryModel
				+ ", desktopModel=" + desktopModel + "]";
	}

	@Override
	public boolean isCorrect() {
		if(!sections.isEmpty() && !options.isEmpty() && !description.isEmpty() && !title.isEmpty()) {
			if (!sections.isBlank() && !options.isBlank() && !description.isBlank() && !title.isBlank()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void createFile() throws IOException {
		//Obtenemos el numero de la seccion, el cual siempre es el primer char del string
		char sectionNumber = this.sections.charAt(0);
		
		//Generamos la ruta donde guardaremos el fichero
		String packageName = controlModel.getPackageName();
		String debPath = debDirectoryModel.getDirectory() + "/deb/usr/share/man/man" + sectionNumber + "/";
		File file = new File(debPath + packageName.toLowerCase() + "." + sectionNumber); //Posteriormente se comprimirá en gz
		
		//Generamos el contenido del fichero man
		Date currentDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
		String strDate = formatter.format(currentDate);
		
		String execCommand = desktopModel.getExecComand();
		
		String fileMsg = ".\\\" Man page for " + packageName.toUpperCase() + ".\n"
				+ ".TH ayacri " + sectionNumber + " \"" + strDate + "\" \n" //\"MATE Desktop Environment\"
				+ ".\\\" Please adjust this date when revising the manpage. \n"
				+ ".\\\" \n"
				+ ".SH \"NAME\" \n"
				+ packageName + "\\- " + this.title + " \n"
				+ ".SH \"SYNOPSIS\" \n"
				+ ".B " + execCommand + " [OPTIONS...] \n"
				+ ".SH \"DESCRIPTION\" \n"
				+ this.description + " \n"
				+ ".PP \n"
				+ "This manual page briefly documents the \\fB" + execCommand + "\\fR command. \n"
				+ ".SH \"OPTIONS\" \n"
				+ ".TP \n"
				+ "\\fB\\-" + this.options + "\\fR \n";
		
		//Escribimos el contenido del fichero
		FileWriter writer = new FileWriter(file);
		writer.write(fileMsg);
		writer.close();
		
		//Comprimimos el ficihero
		ProcessBuilder pb = new ProcessBuilder("gzip","-9n", debPath + packageName.toLowerCase() + "." + sectionNumber);
		Process p = pb.start();
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		p.exitValue();	
	}
	
	/*
	 * Método run
	 * */
	@Override
	public void run() {
		try {
			createFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
		
}
