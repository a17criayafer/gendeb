package models;

import java.io.IOException;

public interface Concurrencia {
	
	/*
	 * Método que comprueba que los campos obligatorios sean correctos y esten correctamente escritos
	 * */
	public boolean isCorrect();
	
	/*
	 * Método que se encargará de crear correctamente los ficheros de una manera concurrente
	 * */
	public void createFile() throws IOException;

}
