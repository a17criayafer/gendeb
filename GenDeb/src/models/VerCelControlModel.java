package models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerCelControlModel extends Thread implements Concurrencia{

	/*
	 * Atributos del fichero Control
	 * */
	private String packageName; 
	private String architecture;
	private String seccion;
	private String priorityControl;
	private String maintainerName;
	private String maintainerEmail;
	private String homePage;
	private String dependences;
	private String description;
	private String version;
	private String source;
	private String essential;	
	private VerVicDebDirectoryModel debDirectoryModel;
	
	/*
	 * Creadoras
	 * */
	public VerCelControlModel() {
		
	}
	
	public VerCelControlModel(String paquete, String architecture, String seccion, String priority, String manteinerName, String manteinerEmail, String homePage, String dependences, String description, String version, String source, String essential) {
		this.architecture = architecture;
		this.dependences = dependences;
		this.description = description;
		this.homePage = homePage;
		this.maintainerName = manteinerName;
		this.maintainerEmail = manteinerEmail;
		this.packageName = paquete;
		this.priorityControl = priority;
		this.seccion = seccion;
		this.version = version;
		this.source = source;
		this.essential = essential;
	}

	/*
	 * Getters de los atributos
	 * */
	
	public String getSeccion() {
		return seccion;
	}
	
	public String getPriorityControl() {
		return priorityControl;
	}
	
	public String getManteinerName() {
		return maintainerName;
	}
	
	public String getManteinerEmail() {
		return maintainerEmail;
	}
	
	public String getHomePage() {
		return homePage;
	}
	
	public String getDependences() {
		return dependences;
	}
	
	public String getDescription() {
		return description;
	}

	public String getPackageName() {
		return packageName;
	}
	
	public String getArchitecture() {
		return architecture;
	}
	
	public String getVersion() {
		return version;
	}
	
	public VerVicDebDirectoryModel getDebDirectoryModel() {
		return debDirectoryModel;
	}
	
	public String getEssential() {
		return essential;
	}
	
	public String getSource() {
		return source;
	}
	
	/*
	 * Setters de los atributos
	 * */	
	
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public void setPriorityControl(String priority) {
		this.priorityControl = priority;
	}
	
	public void setManteinerName(String manteinerName) {
		this.maintainerName = manteinerName;
	}
	
	public void setManteinerEmail(String manteinerEmail) {
		this.maintainerEmail = manteinerEmail;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public void setDependences(String dependences) {
		this.dependences = dependences;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public void setDebDirectoryModel(VerVicDebDirectoryModel debDirectoryModel) {
		this.debDirectoryModel = debDirectoryModel;
	}
	
	public void setEssential(String essential) {
		this.essential = essential;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public void setPaquete(String paquete) {
		this.packageName = paquete;
	}
	
	
	/*
	 * Método toString
	 * */
	@Override
	public String toString() {
		return "VerCelControlModel [packageName=" + packageName + ", architecture=" + architecture + ", seccion="
				+ seccion + ", priorityControl=" + priorityControl + ", maintainerName=" + maintainerName
				+ ", maintainerEmail=" + maintainerEmail + ", homePage=" + homePage + ", dependences=" + dependences
				+ ", description=" + description + ", version=" + version + ", source=" + source + ", essential="
				+ essential + ", debDirectoryModel=" + debDirectoryModel + "]";
	}

	/*
	 * Método isCorrect que mira si los campos obligatorios estan rellenados correctamente
	 * */
	@Override
	public boolean isCorrect() {
		if(!packageName.isEmpty() && !architecture.isEmpty() && !maintainerName.isEmpty() && !maintainerEmail.isEmpty() && !description.isEmpty() && !version.isEmpty()) {
			if (!packageName.isBlank() && !architecture.isBlank() && !maintainerName.isBlank() && !maintainerEmail.isBlank() && !description.isBlank() && !version.isBlank()) {
				String regexEmail = "^(.+)@(.+)$";
				Pattern pattern = Pattern.compile(regexEmail);
				Matcher matcherEmail = pattern.matcher(maintainerEmail);
				
				String regexVersion = "(?!\\.)(\\d+(\\.\\d+)+)(?![\\d\\.])$";
				pattern = Pattern.compile(regexVersion);
				Matcher matcherVersion = pattern.matcher(version);
				if (matcherEmail.matches() && matcherVersion.matches()) {
					return true;
				}
			}
		}	
		return false;
	}

	/*
	 * Método createFile que crea el fichero Control en la carpeta correcta con todos los datos que son obligatorios i los extra necesarios
	 * */
	@Override
	public void createFile() throws IOException {
		//Establecemos el directorio donde se creará el fichero de control
		String debPath = debDirectoryModel.getDirectory() + "/deb/DEBIAN/";
		File file = new File(debPath + "control");
		
		//Creamos el contenido del fichero en un string
		String fileMsg = "Package: " + this.packageName.toLowerCase() + "\n"
				+ "Version: " + this.version + "\n"
				+ "Architecture: " + this.architecture + "\n"
				+ "Maintainer: " + this.maintainerName + " <" + this.maintainerEmail + ">" + "\n"
				+ "Description: " + this.description + "\n";
		
		if (!this.seccion.isEmpty()) fileMsg = fileMsg + "Section: " + this.seccion + "\n";
		if (!this.priorityControl.isEmpty()) fileMsg = fileMsg + "Priority: " + this.priorityControl + "\n";
		if (!this.dependences.isEmpty()) fileMsg = fileMsg + "Depends et al: " + this.dependences + "\n";
		if (!this.homePage.isEmpty()) fileMsg = fileMsg + "Homepage: " + this.homePage + "\n";
		if (!this.source.isEmpty()) fileMsg = fileMsg + "Source: " + this.source + "\n";
		if (!this.essential.isEmpty()) fileMsg = fileMsg + "Essential: " + this.essential + "\n";
		
		//Escribimos el contenido del fichero
		FileWriter writer = new FileWriter(file);
		writer.write(fileMsg);
		writer.close();
	}
	
	/*
	 * Método run
	 * */
	@Override
	public void run() {
		try {
			createFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}
