package models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class VerCelDataJarIconModel extends Thread implements Concurrencia {
	/*
	 * Atributos de la clase
	 * */
	private String iconDirectory;
	private String jarDirectory;
	private VerVicDebDirectoryModel debDirectoryModel;
	private VerCelControlModel controlModel;

	/*
	 * Creadoras
	 * */
	public VerCelDataJarIconModel() {
		
	}
	
	public VerCelDataJarIconModel(String iconDirecotry, String jarDirectory) {
		this.iconDirectory = iconDirecotry;
		this.jarDirectory = jarDirectory;
	}

	/*
	 * Getters de los atributos de la clase
	 * */
	public String getIconDirectory() {
		return iconDirectory;
	}

	public String getJarDirectory() {
		return jarDirectory;
	}
	
	public VerVicDebDirectoryModel getDebDirectoryModel() {
		return debDirectoryModel;
	}
	
	public VerCelControlModel getControlModel() {
		return controlModel;
	}
	
	/*
	 * Setters de los atributos de la clase
	 * */
	
	public void setIconDirectory(String iconDirectory) {
		this.iconDirectory = iconDirectory;
	}

	public void setJarDirectory(String jarDirectory) {
		this.jarDirectory = jarDirectory;
	}
	
	public void setDebDirectoryModel(VerVicDebDirectoryModel debDirectoryModel) {
		this.debDirectoryModel = debDirectoryModel;
	}
	
	public void setControlModel(VerCelControlModel controlModel) {
		this.controlModel = controlModel;
	}

	/*
	 * Método toString
	 * */
	@Override
	public String toString() {
		return "VerCelDataJarIconModel [iconDirectory=" + iconDirectory + ", jarDirectory=" + jarDirectory
				+ ", debDirectoryModel=" + debDirectoryModel + ", controlModel=" + controlModel + "]";
	}

	@Override
	public boolean isCorrect() {
		if(!iconDirectory.isEmpty() && !jarDirectory.isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public void createFile() throws IOException {
		//Generamos la ruta donde guardaremos el fichero
		String packageName = controlModel.getPackageName().toLowerCase();
		String[] jarPath = this.getJarDirectory().split("/");
		String debPath = debDirectoryModel.getDirectory() + "/deb/usr/share/" + packageName + "/";
		
		//Copiamos el fichero jar adjunto
		File originPath = new File(this.getIconDirectory());
		File finalPath = new File(debPath + jarPath[jarPath.length - 1]);
		Files.copy(Paths.get(originPath.getAbsolutePath()), Paths.get(finalPath.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
		
		//Generamos el fichero script
		String jarName = jarPath[jarPath.length - 1];
		String[] jarNameArray = jarName.split("\\.");
		String debPathScript = debDirectoryModel.getDirectory() + "/deb/usr/bin/";
		
		//Escribimos el contenido del fichero
		File file = new File(debPathScript + controlModel.getPackageName().toLowerCase());
		FileWriter writer = new FileWriter(file);
		writer.write("#!/bin/sh \n"
				+ "java -jar /usr/share/" + packageName.toLowerCase() + "/" + jarName + " $@");
		writer.close();
	}
	
	/*
	 * Método run que llama al createFile para usarlo con threads
	 * */
	@Override
	public void run() {
		try {
			createFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}
