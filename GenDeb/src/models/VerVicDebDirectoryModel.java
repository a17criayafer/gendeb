package models;

import java.io.IOException;

public class VerVicDebDirectoryModel implements Concurrencia {
	/*
	 * Atributos necesarios para poder crear la estructura de ficheros
	 * */
	private String directory;
	
	/*
	 * Creadoras
	 * */
	public VerVicDebDirectoryModel() {
		
	}
	
	public VerVicDebDirectoryModel(String directory) {
		this.directory = directory;
	}

	/*
	 * Getters y setters
	 * */
	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	/*
	 * Método toString
	 * */
	@Override
	public String toString() {
		return "DebDirectoryModel [directory=" + directory + "]";
	}

	@Override
	public boolean isCorrect() {
		if(!directory.isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public void createFile() throws IOException {
		
	}
	
}
