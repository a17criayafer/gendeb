package models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.TimeZone;

public class VerVicDocModel extends Thread implements Concurrencia{

	/*
	 * Atributos de la clase
	 * */
	private String urgency;
	private String distributions;
	private String directory;
	private String changes;
	private AyaCriDesktopModel desktopModel;
	private VerCelControlModel controlModel;
	private VerVicDebDirectoryModel debDirectoryModel;

	/*
	 * Creadoras
	 * */
	public VerVicDocModel() {
		
	}
	
	public VerVicDocModel(String urgency, String distributions, String directory, String changes) {
		this.urgency = urgency;
		this.distributions = distributions;
		this.directory = distributions;
		this.changes = changes;
	}

	/*
	 * Getters de los atributos de la clase
	 * */
	public String getUrgency() {
		return urgency;
	}
	
	public String getDistributions() {
		return distributions;
	}
	
	public String getDirectory() {
		return directory;
	}
	
	public String getChanges() {
		return changes;
	}
	
	public AyaCriDesktopModel getDesktopModel() {
		return desktopModel;
	}
	
	public VerCelControlModel getControlModel() {
		return controlModel;
	}
	
	public VerVicDebDirectoryModel getDebDirectoryModel() {
		return debDirectoryModel;
	}
	
	
	/*
	 * Setters de los atributos de la clase
	 * */

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public void setDistributions(String distributions) {
		this.distributions = distributions;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public void setChanges(String changes) {
		this.changes = changes;
	}
	
	public void setDesktopModel(AyaCriDesktopModel desktopModel) {
		this.desktopModel = desktopModel;
	}
	
	public void setControlModel(VerCelControlModel controlModel) {
		this.controlModel = controlModel;
	}

	public void setDebDirectoryModel(VerVicDebDirectoryModel debDirectoryModel) {
		this.debDirectoryModel = debDirectoryModel;
	}

	/*
	 * Método toString
	 * */
	@Override
	public String toString() {
		return "VerVicDocModel [urgency=" + urgency + ", distributions=" + distributions + ", directory=" + directory
				+ ", changes=" + changes + ", desktopModel=" + desktopModel + ", controlModel=" + controlModel
				+ ", debDirectoryModel=" + debDirectoryModel + "]";
	}

	@Override
	public boolean isCorrect() {
		if(!urgency.isEmpty() && !distributions.isEmpty() && !directory.isEmpty() && !changes.isEmpty()) {
			if (!urgency.isBlank() && !distributions.isBlank() && !changes.isBlank()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void createFile() throws IOException {
		//Generamos la ruta donde guardaremos el fichero
		String packageName = controlModel.getPackageName().toLowerCase();
		String debPath = debDirectoryModel.getDirectory() + "/deb/usr/share/doc/" + packageName + "/";
		
		//Copiamos el fichero copyright adjunto
		File originPath = new File(this.getDirectory());
		File finalPath = new File(debPath + "copyright");
		Files.copy(Paths.get(originPath.getAbsolutePath()), Paths.get(finalPath.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
		
		//Dar formato a la fecha del fichero changelog
		DateFormat dateTimeInGMT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ZZ");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ZZ", java.util.Locale.ENGLISH);
		Date myDate = null;
		
		try {
			myDate = sdf.parse(dateTimeInGMT.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		sdf.applyPattern("EEE, d MMM yyyy HH:mm:ss ZZ");
		String sMyDate = sdf.format(myDate);
		
		//Generamos la ruta donde guardaremos el fichero
		debPath = debDirectoryModel.getDirectory() + "/deb/usr/share/doc/" + packageName + "/";
		File file = new File(debPath + "changelog");
		
		//Generamos el contenido del fichero
		FileWriter writer = new FileWriter(file);
		writer.write(desktopModel.getNombre() + " " + controlModel.getVersion() + " " + this.distributions + "; urgency=" + this.urgency + "\n"
				+ "\t" + "* " + this.changes + "\n"
				+ " -- " + controlModel.getManteinerName() + " <" + controlModel.getManteinerEmail() + "> " + sMyDate);		
		writer.close();
		
		//Comprimir fichero changelog
		ProcessBuilder pb = new ProcessBuilder("gzip","-9", "-n", debPath + "changelog");
		Process p = pb.start();
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		p.exitValue();	
	}
	
	/*
	 * Método run que llama al createFile para usarlo con threads
	 * */
	@Override
	public void run() {
		try {
			createFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}
