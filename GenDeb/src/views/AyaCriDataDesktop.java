package views;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import models.AyaCriDesktopModel;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JList;

public class AyaCriDataDesktop extends JPanel {
	private JTextField txtDataDesktopNombre;
	private JTextField txtDataDesktopComentario;
	private JTextField txtDataDesktopExec;
	private JTextField txtDataDesktopType;
	private JTextField txtDataDesktopCategories;
	private JTextField txtDataDesktopTerminal;

	/**
	 * Create the panel.
	 */
	public AyaCriDataDesktop() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel northPanelDataDesktopPanel = new JPanel();
		add(northPanelDataDesktopPanel, BorderLayout.NORTH);
		GridBagLayout gbl_northPanelDataDesktopPanel = new GridBagLayout();
		gbl_northPanelDataDesktopPanel.columnWidths = new int[]{0, 0};
		gbl_northPanelDataDesktopPanel.rowHeights = new int[]{0, 0, 0};
		gbl_northPanelDataDesktopPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northPanelDataDesktopPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		northPanelDataDesktopPanel.setLayout(gbl_northPanelDataDesktopPanel);
		
		JLabel titleDataDesktop = new JLabel("Desktop File");
		titleDataDesktop.setFont(new Font("Dialog", Font.BOLD, 25));
		titleDataDesktop.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_titleDataDesktop = new GridBagConstraints();
		gbc_titleDataDesktop.insets = new Insets(0, 0, 5, 0);
		gbc_titleDataDesktop.anchor = GridBagConstraints.NORTH;
		gbc_titleDataDesktop.gridx = 0;
		gbc_titleDataDesktop.gridy = 0;
		northPanelDataDesktopPanel.add(titleDataDesktop, gbc_titleDataDesktop);
		
		JPanel centerDataDesktopPanel = new JPanel();
		add(centerDataDesktopPanel, BorderLayout.CENTER);
		GridBagLayout gbl_centerDataDesktopPanel = new GridBagLayout();
		gbl_centerDataDesktopPanel.columnWidths = new int[] {0, 150, 250, 0};
		gbl_centerDataDesktopPanel.rowHeights = new int[] {30, 30, 30, 30, 30, 30};
		gbl_centerDataDesktopPanel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0};
		gbl_centerDataDesktopPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		centerDataDesktopPanel.setLayout(gbl_centerDataDesktopPanel);
		
		JLabel lblDataDesktopNombre = new JLabel("Application Name");
		lblDataDesktopNombre.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDataDesktopNombre = new GridBagConstraints();
		gbc_lblDataDesktopNombre.anchor = GridBagConstraints.WEST;
		gbc_lblDataDesktopNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDesktopNombre.gridx = 1;
		gbc_lblDataDesktopNombre.gridy = 0;
		centerDataDesktopPanel.add(lblDataDesktopNombre, gbc_lblDataDesktopNombre);
		
		txtDataDesktopNombre = new JTextField();

		GridBagConstraints gbc_txtDataDesktopNombre = new GridBagConstraints();
		gbc_txtDataDesktopNombre.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataDesktopNombre.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataDesktopNombre.gridx = 2;
		gbc_txtDataDesktopNombre.gridy = 0;
		centerDataDesktopPanel.add(txtDataDesktopNombre, gbc_txtDataDesktopNombre);
		txtDataDesktopNombre.setColumns(10);
		
		JLabel lblDataDesktopComentario = new JLabel("Comment");
		lblDataDesktopComentario.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDataDesktopComentario = new GridBagConstraints();
		gbc_lblDataDesktopComentario.anchor = GridBagConstraints.WEST;
		gbc_lblDataDesktopComentario.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDesktopComentario.gridx = 1;
		gbc_lblDataDesktopComentario.gridy = 1;
		centerDataDesktopPanel.add(lblDataDesktopComentario, gbc_lblDataDesktopComentario);
		
		txtDataDesktopComentario = new JTextField();
		GridBagConstraints gbc_txtDataDesktopComentario = new GridBagConstraints();
		gbc_txtDataDesktopComentario.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataDesktopComentario.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataDesktopComentario.gridx = 2;
		gbc_txtDataDesktopComentario.gridy = 1;
		centerDataDesktopPanel.add(txtDataDesktopComentario, gbc_txtDataDesktopComentario);
		txtDataDesktopComentario.setColumns(10);
		
		JLabel lblDataDesktopExec = new JLabel("Execution Command");
		lblDataDesktopExec.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDataDesktopExec = new GridBagConstraints();
		gbc_lblDataDesktopExec.anchor = GridBagConstraints.WEST;
		gbc_lblDataDesktopExec.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDesktopExec.gridx = 1;
		gbc_lblDataDesktopExec.gridy = 2;
		centerDataDesktopPanel.add(lblDataDesktopExec, gbc_lblDataDesktopExec);
		
		txtDataDesktopExec = new JTextField();
		GridBagConstraints gbc_txtDataDesktopExec = new GridBagConstraints();
		gbc_txtDataDesktopExec.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataDesktopExec.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataDesktopExec.gridx = 2;
		gbc_txtDataDesktopExec.gridy = 2;
		centerDataDesktopPanel.add(txtDataDesktopExec, gbc_txtDataDesktopExec);
		txtDataDesktopExec.setColumns(10);
		
		JLabel lblDataDesktopType = new JLabel("Type");
		lblDataDesktopType.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDataDesktopType = new GridBagConstraints();
		gbc_lblDataDesktopType.anchor = GridBagConstraints.WEST;
		gbc_lblDataDesktopType.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDesktopType.gridx = 1;
		gbc_lblDataDesktopType.gridy = 3;
		centerDataDesktopPanel.add(lblDataDesktopType, gbc_lblDataDesktopType);
		
		txtDataDesktopType = new JTextField();
		GridBagConstraints gbc_txtDataDesktopType = new GridBagConstraints();
		gbc_txtDataDesktopType.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataDesktopType.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataDesktopType.gridx = 2;
		gbc_txtDataDesktopType.gridy = 3;
		centerDataDesktopPanel.add(txtDataDesktopType, gbc_txtDataDesktopType);
		txtDataDesktopType.setColumns(10);
		
		JLabel lblDataDesktopCategories = new JLabel("Categories");
		lblDataDesktopCategories.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDataDesktopCategories = new GridBagConstraints();
		gbc_lblDataDesktopCategories.anchor = GridBagConstraints.WEST;
		gbc_lblDataDesktopCategories.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDesktopCategories.gridx = 1;
		gbc_lblDataDesktopCategories.gridy = 4;
		centerDataDesktopPanel.add(lblDataDesktopCategories, gbc_lblDataDesktopCategories);
		
		txtDataDesktopCategories = new JTextField();
		GridBagConstraints gbc_txtDataDesktopCategories = new GridBagConstraints();
		gbc_txtDataDesktopCategories.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataDesktopCategories.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataDesktopCategories.gridx = 2;
		gbc_txtDataDesktopCategories.gridy = 4;
		centerDataDesktopPanel.add(txtDataDesktopCategories, gbc_txtDataDesktopCategories);
		txtDataDesktopCategories.setColumns(10);
		
		JLabel lblTerminal = new JLabel("Terminal");
		GridBagConstraints gbc_lblTerminal = new GridBagConstraints();
		gbc_lblTerminal.anchor = GridBagConstraints.WEST;
		gbc_lblTerminal.insets = new Insets(0, 0, 0, 5);
		gbc_lblTerminal.gridx = 1;
		gbc_lblTerminal.gridy = 5;
		centerDataDesktopPanel.add(lblTerminal, gbc_lblTerminal);
		
		txtDataDesktopTerminal = new JTextField();
		GridBagConstraints gbc_txtDataDesktopTerminal = new GridBagConstraints();
		gbc_txtDataDesktopTerminal.insets = new Insets(0, 0, 0, 5);
		gbc_txtDataDesktopTerminal.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataDesktopTerminal.gridx = 2;
		gbc_txtDataDesktopTerminal.gridy = 5;
		centerDataDesktopPanel.add(txtDataDesktopTerminal, gbc_txtDataDesktopTerminal);
		txtDataDesktopTerminal.setColumns(10);

	}

	public String getTxtDataDesktopNombreText() {
		return txtDataDesktopNombre.getText();
	}
	public void setTxtDataDesktopNombreText(String text) {
		txtDataDesktopNombre.setText(text);
	}
	public String getTxtDataDesktopComentarioText() {
		return txtDataDesktopComentario.getText();
	}
	public void setTxtDataDesktopComentarioText(String text_1) {
		txtDataDesktopComentario.setText(text_1);
	}
	public String getTxtDataDesktopExecText() {
		return txtDataDesktopExec.getText();
	}
	public void setTxtDataDesktopExecText(String text_2) {
		txtDataDesktopExec.setText(text_2);
	}
	public String getTxtDataDesktopTypeText() {
		return txtDataDesktopType.getText();
	}
	public void setTxtDataDesktopTypeText(String text_3) {
		txtDataDesktopType.setText(text_3);
	}
	public String getTxtDataDesktopCategoriesText() {
		return txtDataDesktopCategories.getText();
	}
	public void setTxtDataDesktopCategoriesText(String text_4) {
		txtDataDesktopCategories.setText(text_4);
	}
	
	public String getTxtDataDesktopTerminal() {
		return txtDataDesktopTerminal.getText();
	}
	public void setTxtDataDesktopTermibal(String text) {
		txtDataDesktopTerminal.setText(text);
	}
}
