package views;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import models.AyaCriManModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class AyaCriDataMan extends JPanel{
	private JTextField txtDataManTitle;
	private JTextField txtDataManOptions;
	private JTextArea txtADataManDescription;
	private JComboBox<String> comboBoxDataManSections;

	/**
	 * Create the panel.
	 */
	public AyaCriDataMan() {		
		setLayout(new BorderLayout(0, 0));
		
		String[] sectionsManUbuntu = {
				"1 - Executable programs",
				"2 - System calls",
				"3 - Library calls",
				"4 - Special files",
				"5 - File formats",
				"6 - Games",
				"7 - Miscellaneous",
				"8 - System administration",
				"9 - Kernel"
		};
		
		JPanel northDataManPanel = new JPanel();
		add(northDataManPanel, BorderLayout.NORTH);
		GridBagLayout gbl_northDataManPanel = new GridBagLayout();
		gbl_northDataManPanel.columnWidths = new int[]{0, 0};
		gbl_northDataManPanel.rowHeights = new int[]{0, 0, 0};
		gbl_northDataManPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northDataManPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		northDataManPanel.setLayout(gbl_northDataManPanel);
		
		JLabel titleDataMan = new JLabel("Manual File");
		titleDataMan.setHorizontalAlignment(SwingConstants.LEFT);
		titleDataMan.setFont(new Font("Dialog", Font.BOLD, 25));
		GridBagConstraints gbc_titleDataMan = new GridBagConstraints();
		gbc_titleDataMan.anchor = GridBagConstraints.NORTH;
		gbc_titleDataMan.insets = new Insets(0, 0, 5, 0);
		gbc_titleDataMan.gridx = 0;
		gbc_titleDataMan.gridy = 0;
		northDataManPanel.add(titleDataMan, gbc_titleDataMan);
		
		JPanel centerDataManPanel = new JPanel();
		add(centerDataManPanel, BorderLayout.CENTER);
		GridBagLayout gbl_centerDataManPanel = new GridBagLayout();
		gbl_centerDataManPanel.columnWidths = new int[] {0, 150, 250, 0};
		gbl_centerDataManPanel.rowHeights = new int[] {30, 30, 30, 30, 60, 30};
		gbl_centerDataManPanel.columnWeights = new double[]{1.0, 0.0, 1.0, 1.0};
		gbl_centerDataManPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0};
		centerDataManPanel.setLayout(gbl_centerDataManPanel);
		
		JLabel lblDataManSections = new JLabel("Sections");
		GridBagConstraints gbc_lblDataManSections = new GridBagConstraints();
		gbc_lblDataManSections.anchor = GridBagConstraints.WEST;
		gbc_lblDataManSections.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataManSections.gridx = 1;
		gbc_lblDataManSections.gridy = 1;
		centerDataManPanel.add(lblDataManSections, gbc_lblDataManSections);
		
		comboBoxDataManSections = new JComboBox<String>();
		comboBoxDataManSections.setModel(new DefaultComboBoxModel<String>(sectionsManUbuntu));
		comboBoxDataManSections.setSelectedIndex(0);
		
		GridBagConstraints gbc_comboBoxDataManSections = new GridBagConstraints();
		gbc_comboBoxDataManSections.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxDataManSections.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxDataManSections.gridx = 2;
		gbc_comboBoxDataManSections.gridy = 1;
		centerDataManPanel.add(comboBoxDataManSections, gbc_comboBoxDataManSections);
		
		JLabel lblDataManTitle = new JLabel("Title sentence");
		GridBagConstraints gbc_lblDataManTitle = new GridBagConstraints();
		gbc_lblDataManTitle.anchor = GridBagConstraints.WEST;
		gbc_lblDataManTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataManTitle.gridx = 1;
		gbc_lblDataManTitle.gridy = 2;
		centerDataManPanel.add(lblDataManTitle, gbc_lblDataManTitle);
		
		txtDataManTitle = new JTextField();
		GridBagConstraints gbc_txtDataManTitle = new GridBagConstraints();
		gbc_txtDataManTitle.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataManTitle.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataManTitle.gridx = 2;
		gbc_txtDataManTitle.gridy = 2;
		centerDataManPanel.add(txtDataManTitle, gbc_txtDataManTitle);
		txtDataManTitle.setColumns(10);
		
		JLabel lblDataManOptions = new JLabel("Options");
		GridBagConstraints gbc_lblDataManOptions = new GridBagConstraints();
		gbc_lblDataManOptions.anchor = GridBagConstraints.WEST;
		gbc_lblDataManOptions.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataManOptions.gridx = 1;
		gbc_lblDataManOptions.gridy = 3;
		centerDataManPanel.add(lblDataManOptions, gbc_lblDataManOptions);
		
		txtDataManOptions = new JTextField();
		GridBagConstraints gbc_txtDataManOptions = new GridBagConstraints();
		gbc_txtDataManOptions.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDataManOptions.insets = new Insets(0, 0, 5, 5);
		gbc_txtDataManOptions.gridx = 2;
		gbc_txtDataManOptions.gridy = 3;
		centerDataManPanel.add(txtDataManOptions, gbc_txtDataManOptions);
		txtDataManOptions.setColumns(10);
		
		JLabel lblDataManDescription = new JLabel("Description");
		GridBagConstraints gbc_lblDataManDescription = new GridBagConstraints();
		gbc_lblDataManDescription.anchor = GridBagConstraints.WEST;
		gbc_lblDataManDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataManDescription.gridx = 1;
		gbc_lblDataManDescription.gridy = 4;
		centerDataManPanel.add(lblDataManDescription, gbc_lblDataManDescription);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 4;
		centerDataManPanel.add(scrollPane, gbc_scrollPane);
		
		txtADataManDescription = new JTextArea();
		txtADataManDescription.setLineWrap(true);
		scrollPane.setViewportView(txtADataManDescription);

	}

	public String getComboBoxDataManSections() {
		return comboBoxDataManSections.getSelectedItem().toString();
	}
	
	public String getTxtDataManTitle() {
		return txtDataManTitle.getText();
	}
	
	public String getTxtDataManOptions() {
		return txtDataManOptions.getText();
	}
	public String getTxtADataManDescription() {
		return txtADataManDescription.getText();
	}
}
