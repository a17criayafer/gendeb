package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.Font;

public class AyaCriErrores extends JPanel {

	/**
	 * Create the panel.
	 */
	public AyaCriErrores() {
		setLayout(new BorderLayout(0, 0));
		
		JLabel lblYoRepasaraTodo = new JLabel("Yo repasaría todo lo de antes...");
		lblYoRepasaraTodo.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblYoRepasaraTodo, BorderLayout.CENTER);
		
		JLabel lblVayaPareceQue = new JLabel("PARECE QUE HAY UN ERROR ");
		lblVayaPareceQue.setFont(new Font("Dialog", Font.BOLD, 24));
		lblVayaPareceQue.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblVayaPareceQue, BorderLayout.NORTH);

	}

}
