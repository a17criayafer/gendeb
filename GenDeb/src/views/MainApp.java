package views;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import controllers.MainController;

public class MainApp {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
									
					VerVicWelcome frame = new VerVicWelcome();
					
					Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
					frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
					
					MainController mainController = new MainController(frame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
