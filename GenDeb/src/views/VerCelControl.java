package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class VerCelControl extends JPanel
{
	private JTextField txtDataManName;
	private JTextField txtDataManTitle;
	private JTextField txtDataManSinopsis;
	private JTextField txtDataManOptions;
	private JTextField txtPackage;
	private JTextField txtArchitecture;
	private JTextField txtMaintainerEmail;
	private JTextField txtHomePage;
	private JTextArea txtADescription;
	private JTextField txtSource;
	private JTextField txtVersion;
	private JTextField txtSection;
	private JTextField txtPriority;
	private JTextField txtEssential;
	private JTextField txtDepends;
	private JTextField txtMaintainerName;

	public VerCelControl() {
		setPreferredSize(new Dimension(600, 472));
		setLayout(new BorderLayout(0, 0));
		
		JPanel northDataContorlPanel = new JPanel();
		add(northDataContorlPanel, BorderLayout.NORTH);
		GridBagLayout gbl_northDataContorlPanel = new GridBagLayout();
		gbl_northDataContorlPanel.columnWidths = new int[]{0, 0};
		gbl_northDataContorlPanel.rowHeights = new int[]{0, 0, 0};
		gbl_northDataContorlPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northDataContorlPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		northDataContorlPanel.setLayout(gbl_northDataContorlPanel);
		
		JLabel titleDataControl = new JLabel("Control File");
		titleDataControl.setHorizontalAlignment(SwingConstants.LEFT);
		titleDataControl.setFont(new Font("Dialog", Font.BOLD, 25));
		GridBagConstraints gbc_titleDataControl = new GridBagConstraints();
		gbc_titleDataControl.anchor = GridBagConstraints.NORTH;
		gbc_titleDataControl.insets = new Insets(0, 0, 5, 0);
		gbc_titleDataControl.gridx = 0;
		gbc_titleDataControl.gridy = 0;
		northDataContorlPanel.add(titleDataControl, gbc_titleDataControl);
		
		JPanel controlCenterPanel = new JPanel();
		add(controlCenterPanel, BorderLayout.CENTER);
		GridBagLayout gbl_controlCenterPanel = new GridBagLayout();
		gbl_controlCenterPanel.columnWidths = new int[] {30, 150, 250, 30};
		gbl_controlCenterPanel.rowHeights = new int[] {30, 30, 30, 30, 30, 30, 30, 30, 30, 0, 30, 30, 60, 30};
		gbl_controlCenterPanel.columnWeights = new double[]{1.0, 0.0, 1.0, 1.0};
		gbl_controlCenterPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0};
		controlCenterPanel.setLayout(gbl_controlCenterPanel);
		
		JLabel lblPackage = new JLabel("Package (*)");
		GridBagConstraints gbc_lblPackage = new GridBagConstraints();
		gbc_lblPackage.fill = GridBagConstraints.VERTICAL;
		gbc_lblPackage.anchor = GridBagConstraints.WEST;
		gbc_lblPackage.insets = new Insets(0, 0, 5, 5);
		gbc_lblPackage.gridx = 1;
		gbc_lblPackage.gridy = 1;
		controlCenterPanel.add(lblPackage, gbc_lblPackage);
		
		txtPackage = new JTextField();
		GridBagConstraints gbc_txtPackage = new GridBagConstraints();
		gbc_txtPackage.insets = new Insets(0, 0, 5, 5);
		gbc_txtPackage.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPackage.gridx = 2;
		gbc_txtPackage.gridy = 1;
		controlCenterPanel.add(txtPackage, gbc_txtPackage);
		txtPackage.setColumns(10);
		
		JLabel lblSource = new JLabel("Source");
		lblSource.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblSource = new GridBagConstraints();
		gbc_lblSource.anchor = GridBagConstraints.WEST;
		gbc_lblSource.insets = new Insets(0, 0, 5, 5);
		gbc_lblSource.gridx = 1;
		gbc_lblSource.gridy = 2;
		controlCenterPanel.add(lblSource, gbc_lblSource);
		
		txtSource = new JTextField();
		GridBagConstraints gbc_txtSource = new GridBagConstraints();
		gbc_txtSource.insets = new Insets(0, 0, 5, 5);
		gbc_txtSource.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSource.gridx = 2;
		gbc_txtSource.gridy = 2;
		controlCenterPanel.add(txtSource, gbc_txtSource);
		txtSource.setColumns(10);
		
		JLabel lblVersion = new JLabel("Version (*)");
		GridBagConstraints gbc_lblVersion = new GridBagConstraints();
		gbc_lblVersion.anchor = GridBagConstraints.WEST;
		gbc_lblVersion.insets = new Insets(0, 0, 5, 5);
		gbc_lblVersion.gridx = 1;
		gbc_lblVersion.gridy = 3;
		controlCenterPanel.add(lblVersion, gbc_lblVersion);
		
		txtVersion = new JTextField();
		GridBagConstraints gbc_txtVersion = new GridBagConstraints();
		gbc_txtVersion.insets = new Insets(0, 0, 5, 5);
		gbc_txtVersion.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtVersion.gridx = 2;
		gbc_txtVersion.gridy = 3;
		controlCenterPanel.add(txtVersion, gbc_txtVersion);
		txtVersion.setColumns(10);
		
		JLabel lblSection = new JLabel("Section");
		GridBagConstraints gbc_lblSection = new GridBagConstraints();
		gbc_lblSection.anchor = GridBagConstraints.WEST;
		gbc_lblSection.insets = new Insets(0, 0, 5, 5);
		gbc_lblSection.gridx = 1;
		gbc_lblSection.gridy = 4;
		controlCenterPanel.add(lblSection, gbc_lblSection);
		
		txtSection = new JTextField();
		txtSection.setColumns(10);
		GridBagConstraints gbc_txtSection = new GridBagConstraints();
		gbc_txtSection.insets = new Insets(0, 0, 5, 5);
		gbc_txtSection.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSection.gridx = 2;
		gbc_txtSection.gridy = 4;
		controlCenterPanel.add(txtSection, gbc_txtSection);
		
		JLabel lblPriority = new JLabel("Priority");
		GridBagConstraints gbc_lblPriority = new GridBagConstraints();
		gbc_lblPriority.anchor = GridBagConstraints.WEST;
		gbc_lblPriority.insets = new Insets(0, 0, 5, 5);
		gbc_lblPriority.gridx = 1;
		gbc_lblPriority.gridy = 5;
		controlCenterPanel.add(lblPriority, gbc_lblPriority);
		
		txtPriority = new JTextField();
		txtPriority.setColumns(10);
		GridBagConstraints gbc_txtPriority = new GridBagConstraints();
		gbc_txtPriority.insets = new Insets(0, 0, 5, 5);
		gbc_txtPriority.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPriority.gridx = 2;
		gbc_txtPriority.gridy = 5;
		controlCenterPanel.add(txtPriority, gbc_txtPriority);
		
		JLabel lblArchitecture = new JLabel("Architecture (*)");
		GridBagConstraints gbc_lblArchitecture = new GridBagConstraints();
		gbc_lblArchitecture.anchor = GridBagConstraints.WEST;
		gbc_lblArchitecture.insets = new Insets(0, 0, 5, 5);
		gbc_lblArchitecture.gridx = 1;
		gbc_lblArchitecture.gridy = 6;
		controlCenterPanel.add(lblArchitecture, gbc_lblArchitecture);
		
		txtArchitecture = new JTextField();
		GridBagConstraints gbc_txtArchitecture = new GridBagConstraints();
		gbc_txtArchitecture.insets = new Insets(0, 0, 5, 5);
		gbc_txtArchitecture.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtArchitecture.gridx = 2;
		gbc_txtArchitecture.gridy = 6;
		controlCenterPanel.add(txtArchitecture, gbc_txtArchitecture);
		txtArchitecture.setColumns(10);
		
		JLabel lblEssential = new JLabel("Essential");
		GridBagConstraints gbc_lblEssential = new GridBagConstraints();
		gbc_lblEssential.anchor = GridBagConstraints.WEST;
		gbc_lblEssential.insets = new Insets(0, 0, 5, 5);
		gbc_lblEssential.gridx = 1;
		gbc_lblEssential.gridy = 7;
		controlCenterPanel.add(lblEssential, gbc_lblEssential);
		
		txtEssential = new JTextField();
		GridBagConstraints gbc_txtEssential = new GridBagConstraints();
		gbc_txtEssential.insets = new Insets(0, 0, 5, 5);
		gbc_txtEssential.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtEssential.gridx = 2;
		gbc_txtEssential.gridy = 7;
		controlCenterPanel.add(txtEssential, gbc_txtEssential);
		txtEssential.setColumns(10);
		
		JLabel lblDependsEtAl = new JLabel("Depends et al");
		GridBagConstraints gbc_lblDependsEtAl = new GridBagConstraints();
		gbc_lblDependsEtAl.anchor = GridBagConstraints.WEST;
		gbc_lblDependsEtAl.insets = new Insets(0, 0, 5, 5);
		gbc_lblDependsEtAl.gridx = 1;
		gbc_lblDependsEtAl.gridy = 8;
		controlCenterPanel.add(lblDependsEtAl, gbc_lblDependsEtAl);
		
		txtDepends = new JTextField();
		txtDepends.setColumns(10);
		GridBagConstraints gbc_txtDepends = new GridBagConstraints();
		gbc_txtDepends.insets = new Insets(0, 0, 5, 5);
		gbc_txtDepends.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDepends.gridx = 2;
		gbc_txtDepends.gridy = 8;
		controlCenterPanel.add(txtDepends, gbc_txtDepends);
		
		JLabel lblMaintainerName = new JLabel("Maintainer Name (*)");
		GridBagConstraints gbc_lblMaintainerName = new GridBagConstraints();
		gbc_lblMaintainerName.anchor = GridBagConstraints.WEST;
		gbc_lblMaintainerName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaintainerName.gridx = 1;
		gbc_lblMaintainerName.gridy = 9;
		controlCenterPanel.add(lblMaintainerName, gbc_lblMaintainerName);
		
		txtMaintainerName = new JTextField();
		GridBagConstraints gbc_txtMaintainerName = new GridBagConstraints();
		gbc_txtMaintainerName.insets = new Insets(0, 0, 5, 5);
		gbc_txtMaintainerName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMaintainerName.gridx = 2;
		gbc_txtMaintainerName.gridy = 9;
		controlCenterPanel.add(txtMaintainerName, gbc_txtMaintainerName);
		txtMaintainerName.setColumns(10);
		
		JLabel lblMaintainerEmail = new JLabel("Maintainer Email (*)");
		GridBagConstraints gbc_lblMaintainerEmail = new GridBagConstraints();
		gbc_lblMaintainerEmail.anchor = GridBagConstraints.WEST;
		gbc_lblMaintainerEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaintainerEmail.gridx = 1;
		gbc_lblMaintainerEmail.gridy = 10;
		controlCenterPanel.add(lblMaintainerEmail, gbc_lblMaintainerEmail);
		
		txtMaintainerEmail = new JTextField();
		GridBagConstraints gbc_txtMaintainerEmail = new GridBagConstraints();
		gbc_txtMaintainerEmail.insets = new Insets(0, 0, 5, 5);
		gbc_txtMaintainerEmail.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMaintainerEmail.gridx = 2;
		gbc_txtMaintainerEmail.gridy = 10;
		controlCenterPanel.add(txtMaintainerEmail, gbc_txtMaintainerEmail);
		txtMaintainerEmail.setColumns(10);
		
		JLabel lblHomepage = new JLabel("HomePage");
		GridBagConstraints gbc_lblHomepage = new GridBagConstraints();
		gbc_lblHomepage.anchor = GridBagConstraints.WEST;
		gbc_lblHomepage.insets = new Insets(0, 0, 5, 5);
		gbc_lblHomepage.gridx = 1;
		gbc_lblHomepage.gridy = 11;
		controlCenterPanel.add(lblHomepage, gbc_lblHomepage);
		
		txtHomePage = new JTextField();
		GridBagConstraints gbc_txtHomePage = new GridBagConstraints();
		gbc_txtHomePage.insets = new Insets(0, 0, 5, 5);
		gbc_txtHomePage.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtHomePage.gridx = 2;
		gbc_txtHomePage.gridy = 11;
		controlCenterPanel.add(txtHomePage, gbc_txtHomePage);
		txtHomePage.setColumns(10);
		
		JLabel lblDescription = new JLabel("Description (*)");
		GridBagConstraints gbc_lblDescription = new GridBagConstraints();
		gbc_lblDescription.anchor = GridBagConstraints.WEST;
		gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescription.gridx = 1;
		gbc_lblDescription.gridy = 12;
		controlCenterPanel.add(lblDescription, gbc_lblDescription);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 12;
		controlCenterPanel.add(scrollPane, gbc_scrollPane);
		
		txtADescription = new JTextArea();
		txtADescription.setLineWrap(true);
		scrollPane.setViewportView(txtADescription);

	}

	
	
	public String getTxtPackage() {
		return txtPackage.getText();
	}
	public String getTxtSource() {
		return txtSource.getText();
	}
	
	public String getTxtVersion() {
		return txtVersion.getText();
	}
	
	public String getTxtSection() {
		return txtSection.getText();
	}
	
	public String getTxtPriority() {
		return txtPriority.getText();
	}
	
	public String getTxtArchitecture() {
		return txtArchitecture.getText();
	}
	
	public String getTxtEssential() {
		return txtEssential.getText();
	}
	
	public String getTxtDepends() {
		return txtDepends.getText();
	}
	
	public String getTxtMaintainerName() {
		return txtMaintainerName.getText();
	}
	
	public String getTxtMaintainerEmail() {
		return txtMaintainerEmail.getText();
	}
	
	public String getTxtHomePage() {
		return txtHomePage.getText();
	}

	public String getTxtADescription() {
		return txtADescription.getText();
	}
}

