package views;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;

import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import views.VerVicWelcome;

import javax.swing.JTextField;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;

public class VerCelDataJarIcon extends JPanel implements ActionListener {
	private JTextField textFieldJarFile;
	private JTextField textFieldIcon;


	public VerCelDataJarIcon() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel northDataJarIconPanel = new JPanel();
		add(northDataJarIconPanel, BorderLayout.NORTH);
		GridBagLayout gbl_northDataJarIconPanel = new GridBagLayout();
		gbl_northDataJarIconPanel.columnWidths = new int[]{0, 0};
		gbl_northDataJarIconPanel.rowHeights = new int[]{0, 0, 0};
		gbl_northDataJarIconPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northDataJarIconPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		northDataJarIconPanel.setLayout(gbl_northDataJarIconPanel);
		
		JLabel titleDataJarIcon = new JLabel("Jar File & Icon");
		titleDataJarIcon.setHorizontalAlignment(SwingConstants.LEFT);
		titleDataJarIcon.setFont(new Font("Dialog", Font.BOLD, 25));
		GridBagConstraints gbc_titleDataJarIcon = new GridBagConstraints();
		gbc_titleDataJarIcon.anchor = GridBagConstraints.NORTH;
		gbc_titleDataJarIcon.insets = new Insets(0, 0, 5, 0);
		gbc_titleDataJarIcon.gridx = 0;
		gbc_titleDataJarIcon.gridy = 0;
		northDataJarIconPanel.add(titleDataJarIcon, gbc_titleDataJarIcon);
		
		JPanel panelJarFile = new JPanel();
		add(panelJarFile, BorderLayout.CENTER);
		GridBagLayout gbl_panelJarFile = new GridBagLayout();
		gbl_panelJarFile.columnWidths = new int[] {30, 250, 150, 30};
		gbl_panelJarFile.rowHeights = new int[] {50, 27, 50, 29, 43, 50};
		gbl_panelJarFile.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE, 1.0};
		gbl_panelJarFile.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		panelJarFile.setLayout(gbl_panelJarFile);
		
		JLabel lblSelectTheJar = new JLabel("Select the jar file");
		lblSelectTheJar.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblSelectTheJar = new GridBagConstraints();
		gbc_lblSelectTheJar.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblSelectTheJar.insets = new Insets(0, 0, 5, 5);
		gbc_lblSelectTheJar.gridx = 1;
		gbc_lblSelectTheJar.gridy = 1;
		panelJarFile.add(lblSelectTheJar, gbc_lblSelectTheJar);
		
		textFieldJarFile = new JTextField();
		textFieldJarFile.setEditable(false);
		textFieldJarFile.setColumns(10);
		GridBagConstraints gbc_textFieldJarFile = new GridBagConstraints();
		gbc_textFieldJarFile.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldJarFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldJarFile.gridx = 1;
		gbc_textFieldJarFile.gridy = 2;
		panelJarFile.add(textFieldJarFile, gbc_textFieldJarFile);
		
		JButton btnBrowseJarFile = new JButton("Browse");
		btnBrowseJarFile.setActionCommand("SearchJarFile");
		btnBrowseJarFile.addActionListener(this);
		GridBagConstraints gbc_btnBrowseJarFile = new GridBagConstraints();
		gbc_btnBrowseJarFile.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowseJarFile.gridx = 2;
		gbc_btnBrowseJarFile.gridy = 2;
		panelJarFile.add(btnBrowseJarFile, gbc_btnBrowseJarFile);
		
		JLabel lblSelectTheIcon = new JLabel("Select the icon");
		lblSelectTheIcon.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblSelectTheIcon = new GridBagConstraints();
		gbc_lblSelectTheIcon.anchor = GridBagConstraints.WEST;
		gbc_lblSelectTheIcon.insets = new Insets(0, 0, 5, 5);
		gbc_lblSelectTheIcon.gridx = 1;
		gbc_lblSelectTheIcon.gridy = 3;
		panelJarFile.add(lblSelectTheIcon, gbc_lblSelectTheIcon);
		
		textFieldIcon = new JTextField();
		textFieldIcon.setEditable(false);
		textFieldIcon.setColumns(10);
		GridBagConstraints gbc_textFieldIcon = new GridBagConstraints();
		gbc_textFieldIcon.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldIcon.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldIcon.gridx = 1;
		gbc_textFieldIcon.gridy = 4;
		panelJarFile.add(textFieldIcon, gbc_textFieldIcon);
		
		JButton btnBrowseIcon = new JButton("Browse");
		btnBrowseIcon.setActionCommand("SearchIcon");
		btnBrowseIcon.addActionListener(this);
		GridBagConstraints gbc_btnBrowseIcon = new GridBagConstraints();
		gbc_btnBrowseIcon.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowseIcon.gridx = 2;
		gbc_btnBrowseIcon.gridy = 4;
		panelJarFile.add(btnBrowseIcon, gbc_btnBrowseIcon);
		

	}
	
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "SearchJarFile":	
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Specify a file to open");   
			
			/*
			 * Filtro para solo poder selecionar imagenes en el JFileChooser
			 */
			FileNameExtensionFilter jarFilter = new FileNameExtensionFilter("Jar files", "jar");

			//Attaching Filter to JFileChooser object
			fileChooser.setFileFilter(jarFilter);

			int userSelection = fileChooser.showOpenDialog(this);

			if (userSelection == JFileChooser.APPROVE_OPTION) {
			    File fileToOpen = fileChooser.getSelectedFile();
			    textFieldJarFile.setText(fileToOpen.getAbsolutePath());
			}
			break;
			case "SearchIcon":	
				JFileChooser fileChooser2 = new JFileChooser();
				fileChooser2.setDialogTitle("Specify a file to open");
				
				/*
				 * Filtro para solo poder selecionar imagenes en el JFileChooser
				 */
				FileNameExtensionFilter imageFilter = new FileNameExtensionFilter(
					    "Image files", ImageIO.getReaderFileSuffixes());

				//Attaching Filter to JFileChooser object
				fileChooser2.setFileFilter(imageFilter);
				
				int userSelection2 = fileChooser2.showOpenDialog(this);

				if (userSelection2 == JFileChooser.APPROVE_OPTION) {
				    File fileToOpen = fileChooser2.getSelectedFile();
				    textFieldIcon.setText(fileToOpen.getAbsolutePath());
				}
				break;
		}
	}

	public String getTextFieldJarFile() {
		return textFieldJarFile.getText();
	}
	public String getTextFieldIcon() {
		return textFieldIcon.getText();
	}
}
