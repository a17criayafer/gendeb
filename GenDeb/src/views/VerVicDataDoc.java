package views;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class VerVicDataDoc extends JPanel {
	private JTextField textFieldDirectory;
	private JComboBox comboBoxUrgency;
	private JComboBox comboBoxDistributions;
	private JTextArea txtAChanges;
	
	private JButton btnExplorar;

	/**
	 * Create the panel.
	 */
	public VerVicDataDoc() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel northDataDocPanel = new JPanel();
		add(northDataDocPanel, BorderLayout.NORTH);
		GridBagLayout gbl_northDataDocPanel = new GridBagLayout();
		gbl_northDataDocPanel.columnWidths = new int[]{0, 0};
		gbl_northDataDocPanel.rowHeights = new int[]{0, 0, 0};
		gbl_northDataDocPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northDataDocPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		northDataDocPanel.setLayout(gbl_northDataDocPanel);
		
		JLabel titleDataDoc = new JLabel("Documentation Files");
		titleDataDoc.setHorizontalAlignment(SwingConstants.LEFT);
		titleDataDoc.setFont(new Font("Dialog", Font.BOLD, 25));
		GridBagConstraints gbc_titleDataDoc = new GridBagConstraints();
		gbc_titleDataDoc.anchor = GridBagConstraints.NORTH;
		gbc_titleDataDoc.insets = new Insets(0, 0, 5, 0);
		gbc_titleDataDoc.gridx = 0;
		gbc_titleDataDoc.gridy = 0;
		northDataDocPanel.add(titleDataDoc, gbc_titleDataDoc);
		
		JPanel dataDocCenterPanel = new JPanel();
		add(dataDocCenterPanel, BorderLayout.CENTER);
		GridBagLayout gbl_dataDocCenterPanel = new GridBagLayout();
		gbl_dataDocCenterPanel.rowHeights = new int[] {0, 30, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_dataDocCenterPanel.columnWidths = new int[] {15, 120, 187, 91, 15};
		gbl_dataDocCenterPanel.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0, 1.0};
		gbl_dataDocCenterPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0};
		dataDocCenterPanel.setLayout(gbl_dataDocCenterPanel);
		
		JLabel lblUrgency = new JLabel("Urgency");
		GridBagConstraints gbc_lblUrgency = new GridBagConstraints();
		gbc_lblUrgency.insets = new Insets(0, 0, 5, 5);
		gbc_lblUrgency.anchor = GridBagConstraints.WEST;
		gbc_lblUrgency.gridx = 1;
		gbc_lblUrgency.gridy = 1;
		dataDocCenterPanel.add(lblUrgency, gbc_lblUrgency);
		
		comboBoxUrgency = new JComboBox();
		comboBoxUrgency.addItem("High");
		comboBoxUrgency.addItem("Medium");
		comboBoxUrgency.addItem("Low");
		comboBoxUrgency.setPreferredSize(new Dimension(32, 10));
		comboBoxUrgency.setSelectedIndex(0);
		GridBagConstraints gbc_comboBoxUrgency = new GridBagConstraints();
		gbc_comboBoxUrgency.gridwidth = 2;
		gbc_comboBoxUrgency.fill = GridBagConstraints.BOTH;
		gbc_comboBoxUrgency.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxUrgency.gridx = 2;
		gbc_comboBoxUrgency.gridy = 1;
		dataDocCenterPanel.add(comboBoxUrgency, gbc_comboBoxUrgency);
		
		JLabel lblDistributions = new JLabel("Distributions");
		lblDistributions.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblDistributions.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblDistributions = new GridBagConstraints();
		gbc_lblDistributions.anchor = GridBagConstraints.WEST;
		gbc_lblDistributions.insets = new Insets(0, 0, 5, 5);
		gbc_lblDistributions.gridx = 1;
		gbc_lblDistributions.gridy = 3;
		dataDocCenterPanel.add(lblDistributions, gbc_lblDistributions);
		
		comboBoxDistributions = new JComboBox();
		comboBoxDistributions.addItem("Stable");
		comboBoxDistributions.addItem("Unestable");
		comboBoxDistributions.setSelectedIndex(0);
		GridBagConstraints gbc_comboBoxDistributions = new GridBagConstraints();
		gbc_comboBoxDistributions.gridwidth = 2;
		gbc_comboBoxDistributions.fill = GridBagConstraints.BOTH;
		gbc_comboBoxDistributions.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxDistributions.gridx = 2;
		gbc_comboBoxDistributions.gridy = 3;
		dataDocCenterPanel.add(comboBoxDistributions, gbc_comboBoxDistributions);
		
		JLabel lblCopyright = new JLabel("Copyright");
		lblCopyright.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblCopyright.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblCopyright = new GridBagConstraints();
		gbc_lblCopyright.anchor = GridBagConstraints.WEST;
		gbc_lblCopyright.insets = new Insets(0, 0, 5, 5);
		gbc_lblCopyright.gridx = 1;
		gbc_lblCopyright.gridy = 5;
		dataDocCenterPanel.add(lblCopyright, gbc_lblCopyright);
		
		textFieldDirectory = new JTextField();
		textFieldDirectory.setEditable(false);
		GridBagConstraints gbc_textFieldDirectory = new GridBagConstraints();
		gbc_textFieldDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldDirectory.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldDirectory.gridx = 2;
		gbc_textFieldDirectory.gridy = 5;
		dataDocCenterPanel.add(textFieldDirectory, gbc_textFieldDirectory);
		textFieldDirectory.setColumns(10);
		
		btnExplorar = new JButton("Explorar");
		btnExplorar.setMaximumSize(new Dimension(32767, 32767));
		btnExplorar.setActionCommand("Explorar");
		GridBagConstraints gbc_btnExplorar = new GridBagConstraints();
		gbc_btnExplorar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnExplorar.insets = new Insets(0, 0, 5, 5);
		gbc_btnExplorar.gridx = 3;
		gbc_btnExplorar.gridy = 5;
		dataDocCenterPanel.add(btnExplorar, gbc_btnExplorar);
		
		JLabel lblChanges = new JLabel("Changes");
		lblChanges.setHorizontalAlignment(SwingConstants.CENTER);
		lblChanges.setAlignmentX(0.5f);
		GridBagConstraints gbc_lblChanges = new GridBagConstraints();
		gbc_lblChanges.anchor = GridBagConstraints.WEST;
		gbc_lblChanges.fill = GridBagConstraints.VERTICAL;
		gbc_lblChanges.insets = new Insets(0, 0, 5, 5);
		gbc_lblChanges.gridx = 1;
		gbc_lblChanges.gridy = 7;
		dataDocCenterPanel.add(lblChanges, gbc_lblChanges);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 2;
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 2;
		gbc_scrollPane.gridy = 7;
		dataDocCenterPanel.add(scrollPane, gbc_scrollPane);
		
		txtAChanges = new JTextArea();
		txtAChanges.setLineWrap(true);
		scrollPane.setViewportView(txtAChanges);

	}

	public String getComboBoxUrgency() {
		return comboBoxUrgency.getSelectedItem().toString();
	}
	public String getComboBoxDistributions() {
		return comboBoxDistributions.getSelectedItem().toString();
	}
	public String getTextFieldDirectory() {
		return textFieldDirectory.getText();
	}
	public JButton getBtnExplorar() {
		return btnExplorar;
	}
	
	public String getTxtAChanges() {
		return txtAChanges.getText();
	}

	public void setTextFieldDirectory(String absolutePath) {
		textFieldDirectory.setText(absolutePath);
	}
}
