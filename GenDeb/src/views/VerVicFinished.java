package views;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class VerVicFinished extends JPanel {

	/**
	 * Create the panel.
	 */
	public VerVicFinished() {
		setLayout(new BorderLayout(0, 0));
		
		JLabel lblElPaqueteSe = new JLabel("Your package has been created correctly.");
		lblElPaqueteSe.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblElPaqueteSe, BorderLayout.CENTER);

	}

}
