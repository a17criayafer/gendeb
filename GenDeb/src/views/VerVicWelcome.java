package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JToggleButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class VerVicWelcome extends JFrame {

	private JPanel contentPane;
	private JPanel centerPanel;
	private JButton btnNext;
	private JButton btnBack;
	private JButton btnClose;
	public int cardActual = 0;
	private AyaCriDataDesktop ayaCriDataDesktop;
	private VerCelDataJarIcon verCelDataJarIcon;
	private AyaCriDataMan ayaCriDataMan;
	private VerVicDataDoc verVicDataDoc;
	private VerVicWhereToSave verVicDondeGuardar;
	private VerVicFinished verVicFinalizar;
	private JScrollPane scrollPane;
	private VerCelControl verCelControl;


	/**
	 * Create the frame.
	 */
	public VerVicWelcome() {
		setResizable(false);
		
		setTitle("GedDeb");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setPreferredSize(new Dimension(650, 400));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel southButtons = new JPanel();
		contentPane.add(southButtons, BorderLayout.SOUTH);
		southButtons.setLayout(new BorderLayout(0, 0));
		
		JPanel eastButtonPanel = new JPanel();
		southButtons.add(eastButtonPanel, BorderLayout.EAST);
		
		centerPanel = new JPanel();
		contentPane.add(centerPanel, BorderLayout.CENTER);
		CardLayout cl = new CardLayout();
		centerPanel.setLayout(cl);
		
		scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(500, 300));
		scrollPane.setMaximumSize(new Dimension(500, 300));
		scrollPane.getVerticalScrollBar().setUnitIncrement(10); //Increment scroll speed
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		centerPanel.add(scrollPane, "name_1978130123853");
		
		verCelControl = new VerCelControl();
		scrollPane.setViewportView(verCelControl);
		
		verCelDataJarIcon = new VerCelDataJarIcon();
		centerPanel.add(verCelDataJarIcon, "name_6780518196177");
		
		ayaCriDataDesktop = new AyaCriDataDesktop();
		centerPanel.add(ayaCriDataDesktop, "dataDesktop");
		
		ayaCriDataMan = new AyaCriDataMan();
		centerPanel.add(ayaCriDataMan, "dataMan");
		
		verVicDataDoc = new VerVicDataDoc();
		centerPanel.add(verVicDataDoc, "dataDoc");
		
		verVicDondeGuardar = new VerVicWhereToSave();
		centerPanel.add(verVicDondeGuardar, "dondeGuardar");
		
		verVicFinalizar = new VerVicFinished();
		centerPanel.add(verVicFinalizar, "name_7320134876496");
		
		btnBack = new JButton("<- Back");
		btnBack.setActionCommand("Back");
		eastButtonPanel.add(btnBack);
		
		//Por defecto, cuando arranca el programa el boton "Back" se desactiva.
		btnBack.setEnabled(false);
		btnNext = new JButton("Next ->");
		btnNext.setToolTipText("Fill in the required fields");
		btnNext.setActionCommand("Next");
		eastButtonPanel.add(btnNext);
		
		JPanel westButtonPanel = new JPanel();
		southButtons.add(westButtonPanel, BorderLayout.WEST);
		
		btnClose = new JButton("Close");
		btnClose.setToolTipText("Close the application");
		btnClose.setActionCommand("Close");
		westButtonPanel.add(btnClose);
		
		pack();
	}

	public AyaCriDataDesktop getAyaCriDataDesktop() {
		return ayaCriDataDesktop;
	}
	public AyaCriDataMan getAyaCriDataMan() {
		return ayaCriDataMan;
	}
	public VerVicDataDoc getVerVicDataDoc() {
		return verVicDataDoc;
	}
	public VerVicWhereToSave getVerVicDondeGuardar() {
		return verVicDondeGuardar;
	}
	
	public JButton getBtnNext() {
		return btnNext;
	}
	public JPanel getCenterPanel() {
		return centerPanel;
	}
	public JButton getBtnBack() {
		return btnBack;
	}
	public JButton getBtnClose() {
		return btnClose;
	}
	
	public VerCelControl getVerCelControl() {
		return verCelControl;
	}
	public VerCelDataJarIcon getVerCelDataJarIcon() {
		return verCelDataJarIcon;
	}
}
