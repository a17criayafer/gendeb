package views;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextArea;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class VerVicWhereToSave extends JPanel {
	private JTextField textFieldDirectory;
	private JButton btnExplorar;

	/**
	 * Create the panel.
	 */
	public VerVicWhereToSave() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel saveCenterPanel = new JPanel();
		add(saveCenterPanel, BorderLayout.CENTER);
		GridBagLayout gbl_saveCenterPanel = new GridBagLayout();
		gbl_saveCenterPanel.columnWidths = new int[] {30, 250, 150, 30};
		gbl_saveCenterPanel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE, 1.0};
		gbl_saveCenterPanel.rowWeights = new double[]{0.0, 0.0};
		saveCenterPanel.setLayout(gbl_saveCenterPanel);
		
		JLabel lblDirectori = new JLabel("Select the directory");
		lblDirectori.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblDirectori = new GridBagConstraints();
		gbc_lblDirectori.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDirectori.insets = new Insets(0, 0, 5, 5);
		gbc_lblDirectori.gridx = 1;
		gbc_lblDirectori.gridy = 0;
		saveCenterPanel.add(lblDirectori, gbc_lblDirectori);
		
		textFieldDirectory = new JTextField();
		textFieldDirectory.setEditable(false);
		GridBagConstraints gbc_textFieldDirectory = new GridBagConstraints();
		gbc_textFieldDirectory.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldDirectory.insets = new Insets(0, 0, 0, 5);
		gbc_textFieldDirectory.gridx = 1;
		gbc_textFieldDirectory.gridy = 1;
		saveCenterPanel.add(textFieldDirectory, gbc_textFieldDirectory);
		textFieldDirectory.setColumns(10);
		
		btnExplorar = new JButton("Explorar");
		btnExplorar.setActionCommand("Explorar");
		GridBagConstraints gbc_btnExplorar = new GridBagConstraints();
		gbc_btnExplorar.insets = new Insets(0, 0, 0, 5);
		gbc_btnExplorar.gridx = 2;
		gbc_btnExplorar.gridy = 1;
		saveCenterPanel.add(btnExplorar, gbc_btnExplorar);
		
		JPanel northSavePanel = new JPanel();
		add(northSavePanel, BorderLayout.NORTH);
		GridBagLayout gbl_northSavePanel = new GridBagLayout();
		gbl_northSavePanel.columnWidths = new int[]{0, 0};
		gbl_northSavePanel.rowHeights = new int[]{0, 0, 0};
		gbl_northSavePanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northSavePanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		northSavePanel.setLayout(gbl_northSavePanel);
		
		JLabel titleSave = new JLabel("Directory to save your application");
		titleSave.setHorizontalAlignment(SwingConstants.LEFT);
		titleSave.setFont(new Font("Dialog", Font.BOLD, 25));
		GridBagConstraints gbc_titleSave = new GridBagConstraints();
		gbc_titleSave.anchor = GridBagConstraints.NORTH;
		gbc_titleSave.insets = new Insets(0, 0, 5, 0);
		gbc_titleSave.gridx = 0;
		gbc_titleSave.gridy = 0;
		northSavePanel.add(titleSave, gbc_titleSave);

	}

	public String getTextFieldDirectory() {
		return textFieldDirectory.getText();
	}
	public void setTextFieldDirectory(String directory) {
		textFieldDirectory.setText(directory);
	}

	public JButton getBtnExplorar() {
		return btnExplorar;
	}
}
