package models;

import static org.junit.jupiter.api.Assertions.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

class VerVicDocModelTest {


	@Test
	void testGetDate() {
		DateFormat dateTimeInGMT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ZZ");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ZZ", java.util.Locale.ENGLISH);
		Date myDate=null;
		try {
			myDate = sdf.parse(dateTimeInGMT.format(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdf.applyPattern("EEE, d MMM yyyy HH:mm:ss ZZ");
		String sMyDate = sdf.format(myDate);
		
		assertEquals("Thu, 6 Feb 2020 10:11:05 +0100", sMyDate);
	}

}
